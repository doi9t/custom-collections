/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.maps;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface MultiValueMap<K, V> extends Serializable {

    int size();

    boolean containsKey(K key);

    /**
     * Method to check if the key has a specific value.
     *
     * @param item - The item to be checked
     * @return A Set of key(s) where the item has been found, an empty Set otherwise
     */
    Set<K> containsValue(V item);

    /**
     * Method to fetch the values from a key.
     *
     * @param key - The key to be used
     * @return An unmodifiable List containing the values associated to the key, or null if the value is not present
     */
    Collection<V> get(K key);

    /**
     * Method that adds a value to the specific key.
     *
     * @param key   The key
     * @param value The value
     */
    void put(K key, V value);

    /**
     * Method that adds a key with not values (Empty list).
     *
     * @param key The key
     */
    void put(K key);

    /**
     * Remove the key and the values.
     *
     * @param key - The key to be removed
     * @return The List of values associated with the key to be removed
     */
    Collection<V> remove(K key);

    /**
     * Method to remove a specific value from a key
     *
     * @param key   - The key of the values
     * @param value - The value to be removed
     * @return
     */
    Collection<V> removeFrom(K key, V value);

    /**
     * Method to add multiple values to the key.
     *
     * @param m - The map to be merged with the current
     */
    void putAll(MultiValueMap<K, V> m);

    void putAll(K key, Collection<V> collection);

    Set<Map.Entry<K, Collection<V>>> entrySet();

    void clear();

    /**
     * Method to removes the values for a specific key.
     *
     * @param key The key
     */
    void clear(K key);

    Set<K> keySet();

    /**
     * @return Method to return the values of all collections
     */
    Collection<V> values();

    boolean isEmpty();
}
