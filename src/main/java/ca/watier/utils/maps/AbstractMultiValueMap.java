/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.maps;


import java.util.*;

/**
 * Base class for the collections, this is not synchronized and is not thread-safe with the current implementation.
 */
public abstract class AbstractMultiValueMap<K, V> implements MultiValueMap<K, V> {

    private static final long serialVersionUID = -4862594893633125592L;

    protected final Map<K, Collection<V>> containerMap;

    public AbstractMultiValueMap(int defaultMapSize) {
        containerMap = new HashMap<>(defaultMapSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerMap);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractMultiValueMap<?, ?> that = (AbstractMultiValueMap<?, ?>) o;
        return containerMap.equals(that.containerMap);
    }

    @Override
    public int size() {
        return containerMap.size();
    }

    @Override
    public boolean containsKey(K key) {
        return containerMap.containsKey(key);
    }

    @Override
    public Set<K> containsValue(V item) {
        Set<K> value = new HashSet<>();

        for (Map.Entry<K, Collection<V>> collectionEntry : containerMap.entrySet()) {
            if (collectionEntry.getValue().contains(item)) {
                value.add(collectionEntry.getKey());
            }
        }

        return value;
    }

    @Override
    public Collection<V> get(K key) {
        Collection<V> collection = containerMap.get(key);

        if (collection != null) {
            return Collections.unmodifiableCollection(collection);
        }

        return null;
    }

    @Override
    public void put(K key, V value) {
        getAssociation(key).add(value);
    }

    /**
     * Method that returns the inner collection of the value, add a new collection if not present.
     *
     * @param key - The key
     * @return The collection associated to the current key
     */
    protected abstract Collection<V> getAssociation(K key);

    @Override
    public abstract void put(K key);

    @Override
    public Collection<V> remove(K key) {
        return containerMap.remove(key);
    }

    @Override
    public Collection<V> removeFrom(K key, V value) {
        Collection<V> values = containerMap.get(key);

        if (values != null && !values.isEmpty()) {
            values.remove(value);
        }

        return values;
    }

    @Override
    public void putAll(MultiValueMap<K, V> m) {
        if (m == null || m.isEmpty()) {
            return;
        }

        for (Map.Entry<? extends K, ? extends Collection<V>> entry : m.entrySet()) {
            getAssociation(entry.getKey()).addAll(entry.getValue());
        }
    }

    @Override
    public void putAll(K key, Collection<V> collection) {
        if (collection == null || collection.isEmpty()) {
            return;
        }

        getAssociation(key).addAll(collection);
    }

    @Override
    public Set<Map.Entry<K, Collection<V>>> entrySet() {
        return containerMap.entrySet();
    }

    @Override
    public void clear() {
        containerMap.clear();
    }

    @Override
    public void clear(K key) {
        getAssociation(key).clear();
    }

    @Override
    public Set<K> keySet() {
        return containerMap.keySet();
    }

    @Override
    public abstract Collection<V> values();

    @Override
    public boolean isEmpty() {
        return containerMap.isEmpty();
    }
}
