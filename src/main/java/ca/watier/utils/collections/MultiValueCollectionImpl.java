/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.collections;

import ca.watier.utils.models.KeyValueHolder;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * This implementation is the equivalent of having a {@link java.util.ArrayList} with objects containing {@link java.util.ArrayList} as values.
 * <br/><b>This collection is not synchronized and is not thread-safe with the current implementation.<b/>
 */
public class MultiValueCollectionImpl<K, V> implements MultiValueCollection<K, V> {
    private static final long serialVersionUID = 4279577913407671801L;
    protected Collection<KeyValueHolder<K, V>> containerCollection;

    private MultiValueCollectionImpl(Collection<KeyValueHolder<K, V>> collection) {
        this.containerCollection = collection;
    }

    public static <K, V> MultiValueCollection<K, V> arrayList() {
        return MultiValueCollectionImpl.arrayList(10);
    }

    public static <K, V> MultiValueCollection<K, V> arrayList(int initialCapacity) {
        return new MultiValueCollectionImpl<K, V>(new ArrayList<>(initialCapacity));
    }

    public static <K, V> MultiValueCollection<K, V> hashSet() {
        return MultiValueCollectionImpl.hashSet(10);
    }

    public static <K, V> MultiValueCollection<K, V> hashSet(int initialCapacity) {
        return new MultiValueCollectionImpl<K, V>(new HashSet<>(initialCapacity));
    }

    public static <K, V> MultiValueCollection<K, V> linkedBlockingQueue() {
        return MultiValueCollectionImpl.linkedBlockingQueue(10);
    }

    public static <K, V> MultiValueCollection<K, V> linkedBlockingQueue(int initialCapacity) {
        return new MultiValueCollectionImpl<K, V>(new LinkedBlockingQueue<>(initialCapacity));
    }

    public static <K, V> MultiValueCollection<K, V> arrayDeque() {
        return MultiValueCollectionImpl.arrayDeque(10);
    }

    public static <K, V> MultiValueCollection<K, V> arrayDeque(int initialCapacity) {
        return new MultiValueCollectionImpl<K, V>(new ArrayDeque<>(initialCapacity));
    }

    public static <K, V> MultiValueCollection<K, V> linkedList() {
        return new MultiValueCollectionImpl<K, V>(new LinkedList<>());
    }

    @Override
    public Iterator<KeyValueHolder<K, V>> iterator() {
        return containerCollection.iterator();
    }

    @Override
    public void forEach(Consumer<? super KeyValueHolder<K, V>> action) {
        containerCollection.forEach(action);
    }

    @Override
    public Spliterator<KeyValueHolder<K, V>> spliterator() {
        return containerCollection.spliterator();
    }

    @Override
    public int size() {
        return containerCollection.size();
    }

    @Override
    public boolean containsKey(K key) {
        return !get(key).isEmpty();
    }

    @Override
    public Collection<Collection<V>> get(K key) {
        Collection<Collection<V>> values = new ArrayList<>();

        for (KeyValueHolder<K, V> currentHolder : containerCollection) {
            if (Objects.equals(currentHolder.getKey(), key)) {
                values.add(currentHolder.getValues());
            }
        }

        return values;
    }

    @Override
    public void add(K key, Collection<V> value) {
        KeyValueHolder<K, V> kvKeyValueHolder = new KeyValueHolder<>(10);
        kvKeyValueHolder.setKey(key);
        kvKeyValueHolder.addAll(value);
        containerCollection.add(kvKeyValueHolder);
    }

    @SafeVarargs
    @Override
    public final void add(K key, final V... value) {
        if (value == null || value.length == 0) {
            return;
        }

        add(key, Arrays.asList(value));
    }

    @Override
    public void add(K key) {
        KeyValueHolder<K, V> kvKeyValueHolder = new KeyValueHolder<>(10);
        kvKeyValueHolder.setKey(key);
        containerCollection.add(kvKeyValueHolder);
    }

    @Override
    public Collection<Collection<V>> remove(K key) {
        Collection<Collection<V>> values = new ArrayList<>();
        Collection<KeyValueHolder<K, V>> inner = new ArrayList<>();

        for (KeyValueHolder<K, V> currentHolder : containerCollection) {
            if (Objects.equals(currentHolder.getKey(), key)) {
                inner.add(currentHolder);
                values.add(currentHolder.getValues());
            }
        }

        containerCollection.removeAll(inner);

        return values;
    }

    @Override
    public void clear() {
        containerCollection.clear();
    }

    @Override
    public void clear(K key) {
        for (KeyValueHolder<K, V> currentHolder : containerCollection) {
            if (Objects.equals(currentHolder.getKey(), key)) {
                currentHolder.clear();
            }
        }
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = new HashSet<>();

        for (KeyValueHolder<K, V> currentHolder : containerCollection) {
            keys.add(currentHolder.getKey());
        }

        return keys;
    }

    @Override
    public Collection<V> values() {
        Collection<V> values = new ArrayList<>();

        for (KeyValueHolder<K, V> currentHolder : containerCollection) {
            values.addAll(currentHolder.getValues());
        }

        return values;
    }

    @Override
    public boolean isEmpty() {
        return containerCollection.isEmpty();
    }
}
