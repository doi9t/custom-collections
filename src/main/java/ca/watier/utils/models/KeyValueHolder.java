/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.models;

import java.io.Serializable;
import java.util.*;

public class KeyValueHolder<K, V> implements Serializable {
    private static final long serialVersionUID = -5337570603760417025L;

    private final List<V> values;
    private K key;

    /**
     * @param capacity The default size of the values, for the inner collection.
     */
    public KeyValueHolder(int capacity) {
        this.values = new ArrayList<>(capacity);
    }

    public void addAll(Collection<V> value) {
        if (value == null || value.isEmpty()) {
            return;
        }

        values.addAll(value);
    }

    public void clear() {
        values.clear();
    }

    @Override
    public int hashCode() {
        return Objects.hash(values, key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyValueHolder<?, ?> that = (KeyValueHolder<?, ?>) o;
        return values.equals(that.values) && key.equals(that.key);
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = Objects.requireNonNull(key, "The key cannot be null!");
    }

    public List<V> getValues() {
        return Collections.unmodifiableList(values);
    }
}
